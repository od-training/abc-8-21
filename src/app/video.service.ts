import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormControl } from '@angular/forms';
import { combineLatest, Observable, Subject } from 'rxjs';
import { map, startWith } from 'rxjs/operators';

import { Video } from './app.types';

@Injectable({
  providedIn: 'root',
})
export class VideoService {
  videoList: Observable<Video[]>;
  filteredVideoList: Observable<Video[]>;
  selectedVideo: Observable<Video>;
  userChoice: Subject<Video> = new Subject();

  searchControl = new FormControl('');

  constructor(http: HttpClient) {
    this.videoList = http.get<Video[]>(
      'https://api.angularbootcamp.com/videos'
    );

    const validSearchChanges = this.searchControl.valueChanges.pipe(
      startWith(this.searchControl.value)
    );

    this.filteredVideoList = combineLatest([
      this.videoList,
      validSearchChanges,
    ]).pipe(
      map(([list, term]) => list.filter((video) => video.title.includes(term)))
    );

    this.selectedVideo = combineLatest([
      this.filteredVideoList,
      this.userChoice,
      /**Some observable representing the user selection */
    ]).pipe(
      map(([list, userChoice]) => {
        // Return the video if found in the list
        // otherwise just use the first entry in the list
        return list.find((video) => video.id === userChoice.id) || list[0];
      })
    );
  }

  updateSelectedVideo(video: Video){
    this.userChoice.next(video);
  }
}
