import { Component } from '@angular/core';

import { Video } from '../../app.types';
import { VideoService } from '../../video.service';

@Component({
  selector: 'abc-video-dashboard',
  templateUrl: './video-dashboard.component.html',
  styleUrls: ['./video-dashboard.component.scss'],
})
export class VideoDashboardComponent {
  videos = this.videoService.filteredVideoList;
  selectedVideo = this.videoService.selectedVideo;

  constructor(private videoService: VideoService) {}

  updateSelectedVideo(selectedVideo: Video) {
    this.videoService.updateSelectedVideo(selectedVideo);
  }
}
