import { Component, Input, OnInit } from '@angular/core';

import { Video } from '../../app.types';

@Component({
  selector: 'abc-video-player',
  templateUrl: './video-player.component.html',
  styleUrls: ['./video-player.component.scss'],
})
export class VideoPlayerComponent implements OnInit {
  @Input() video: Video | undefined;
  constructor() {}

  ngOnInit(): void {}
}
