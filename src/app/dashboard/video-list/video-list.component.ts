import { Component, EventEmitter, Input, Output } from '@angular/core';

import { Video } from '../../app.types';

@Component({
  selector: 'abc-video-list',
  templateUrl: './video-list.component.html',
  styleUrls: ['./video-list.component.scss'],
})
export class VideoListComponent {
  @Input() videos: Video[] = [];
  @Input() selectedVideoId: string = '';
  @Output() videoSelected = new EventEmitter<Video>();

  setSelectedVideo(video: Video) {
    this.videoSelected.emit(video);
  }
}
