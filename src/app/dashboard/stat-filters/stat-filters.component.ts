import { Component } from '@angular/core';
import { FormControl } from '@angular/forms';
import { VideoService } from 'src/app/video.service';

@Component({
  selector: 'abc-stat-filters',
  templateUrl: './stat-filters.component.html',
  styleUrls: ['./stat-filters.component.scss'],
})
export class StatFiltersComponent {
  searchControl = this.videoService.searchControl;

  constructor(private videoService: VideoService) {}
}
